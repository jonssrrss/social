$('button[name=send]').on('click', function() {
    messager.send(
        $('input[name=message]').val(),
        $('input[name=id]').val()
    );
    $('input[name=message]').val('');
    $('input[name=message]').focus();
});

$(function() {
    $('input[name=message]').focus();
});


$('input[name=message]').keydown(function(e) {
    if (e.keyCode === 13) {
        messager.send(
            $('input[name=message]').val(),
            $('input[name=id]').val()
        );
        $('input[name=message]').val('');
    }
});