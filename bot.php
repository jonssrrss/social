<?
	/*
	* Этот файл для связки с ботом
	*/


	require($_SERVER['DOCUMENT_ROOT'].'/DB.php');
	require($_SERVER['DOCUMENT_ROOT'].'/jxon-taxi.php');

// 	echo json_encode(
// 		array(
// 			'message' => DB::select(
// 				'
// 					SELECT
// 						*
// 					FROM
// 						taxi
// 				'
// 			)
// 		)
// 	);
	// проверяем, есть ли сообщение и id
	if (
		isset($_POST['message']) &&
		isset($_POST['id']) &&
		$_POST['message'] != ''
	) {
		$bot = new JxonTaxi($_POST['id']);
		$bot = $bot->init($_POST['message']);
		echo json_encode($bot);
	} else {
		echo json_encode(array('message' => 'Ошибка'));
	}