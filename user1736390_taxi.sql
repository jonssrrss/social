-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Янв 11 2021 г., 23:37
-- Версия сервера: 5.5.60-MariaDB-cll-lve
-- Версия PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `user1736390_taxi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_success` int(11) NOT NULL COMMENT 'завершен ли заказ? 0 - нет, 1 -да',
  `order_active` int(11) NOT NULL COMMENT '0 - нет\r\n1 - да'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_success`, `order_active`) VALUES
(2, 2, 0, 0),
(3, 10, 0, 0),
(9, 2, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order_list`
--

CREATE TABLE IF NOT EXISTS `order_list` (
  `list_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `list_address_name` text NOT NULL,
  `list_address_lat` text NOT NULL,
  `list_address_lon` text NOT NULL,
  `list_type` int(11) NOT NULL COMMENT 'тип точки (1 - начальная точка, 100 - конечная)'
) ENGINE=InnoDB AUTO_INCREMENT=222 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_list`
--

INSERT INTO `order_list` (`list_id`, `order_id`, `list_address_name`, `list_address_lat`, `list_address_lon`, `list_type`) VALUES
(15, 2, 'Респ Башкортостан, г Уфа, ул Генерала Кусимова, 17', '54.715603', '55.842915', 1),
(17, 2, 'Респ Башкортостан, село Шемяк, ул Молодежная, 13', '54.7870501', '55.4980485', 100),
(216, 7, 'Респ Башкортостан, деревня Якты-Ялан', '54.291128', '55.734373', 1),
(217, 7, 'Респ Башкортостан, г Уфа', '54.734856', '55.957856', 100),
(220, 9, 'Респ Башкортостан, г Уфа, ул Степана Кувыкина, 18', '54.707687', '55.99978', 1),
(221, 9, 'Респ Башкортостан, г Уфа, ул Академика Королева, 27', '54.7727999', '56.0802147', 100);

-- --------------------------------------------------------

--
-- Структура таблицы `sendOrdersToTaxi`
--

CREATE TABLE IF NOT EXISTS `sendOrdersToTaxi` (
  `send_id` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  `id_taxi` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `send_response` int(11) NOT NULL COMMENT 'ответ от таксиста 0 - по умолчанию, 1 - отказ, 2 - взял в работу'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sendOrdersToTaxi`
--

INSERT INTO `sendOrdersToTaxi` (`send_id`, `send_time`, `id_taxi`, `id_order`, `send_response`) VALUES
(1, 1610399637, 1, 9, 0),
(2, 1610399686, 1, 9, 0),
(3, 1610400017, 1, 9, 0),
(4, 1610400032, 1, 9, 0),
(5, 1610400059, 1, 9, 0),
(6, 1610400074, 1, 9, 0),
(7, 1610400124, 1, 9, 0),
(8, 1610400165, 1, 9, 0),
(9, 1610400178, 1, 9, 0),
(10, 1610400951, 1, 9, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `taxi`
--

CREATE TABLE IF NOT EXISTS `taxi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `auto_model` text NOT NULL,
  `auto_mark` text NOT NULL,
  `auto_color` text NOT NULL,
  `auto_numbers` text NOT NULL,
  `confirmed` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `taxi`
--

INSERT INTO `taxi` (`id`, `user_id`, `auto_model`, `auto_mark`, `auto_color`, `auto_numbers`, `confirmed`, `active`, `lat`, `lon`) VALUES
(2, 3, '2', '1', '3', '2', 1, 0, 0, 0),
(3, 1, 'Mark2', 'Toyota', 'Серебристый', 'М602ВК86', 1, 1, 54.702378, 55.994155);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL,
  `user_name` text NOT NULL,
  `user_phone` text NOT NULL,
  `user_tg_id` int(11) NOT NULL,
  `user_nick` text NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_phone`, `user_tg_id`, `user_nick`, `user_status`) VALUES
(1, 'Евгений', '79173597343', 371192189, '', 31),
(2, 'Ильдус', '89608045554', 181855303, '', 6),
(3, 'Фаниль', '66844', 285935356, '', 3);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Индексы таблицы `sendOrdersToTaxi`
--
ALTER TABLE `sendOrdersToTaxi`
  ADD PRIMARY KEY (`send_id`);

--
-- Индексы таблицы `taxi`
--
ALTER TABLE `taxi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT для таблицы `order_list`
--
ALTER TABLE `order_list`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=222;
--
-- AUTO_INCREMENT для таблицы `sendOrdersToTaxi`
--
ALTER TABLE `sendOrdersToTaxi`
  MODIFY `send_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `taxi`
--
ALTER TABLE `taxi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
