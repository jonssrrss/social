<?
	class DB {

		public $paramsPath = 'dbParams.php';
		public $params;
		public $db;

		function __construct() {
			$this->params = include($this->paramsPath);
			$dsn = "mysql:host={$this->params['host']};dbname={$this->params['dbname']};charset=utf8";
			if (!($this->db = new PDO($dsn, $this->params['user'], $this->params['password']))) {
				$this->paramsPath = 'dbParams2.php';
				$this->params = include($this->paramsPath);
				$dsn = "mysql:host={$this->params['host']};dbname={$this->params['dbname']};charset=utf8";
				$this->db = new PDO($dsn, $this->params['user'], $this->params['password']);
			}
		}

		public function select($query) {
			if ($this->inc()) {
				$arr = array();
				$result = $this->db->query($query);
				$result->setFetchMode(PDO::FETCH_ASSOC);
				$i = 0;
				while ($row = $result->fetch()) {
					$arr[$i] = $row;
					$i++;
				}
				return $arr;
			}
		}

		public function insert($query) {
			if ($this->inc()) {
				$result = $this->db->query($query);
				return $this->db->lastInsertId();
			}
		}

		// Считаем количество запросов
		public function inc() {
			$hours = floor(time()/3600);
			$logFilePath = $_SERVER['DOCUMENT_ROOT'].'/sql.txt';
			if (!file_exists($logFilePath)) {
				$fp = fopen($logFilePath, "w");
				fclose($fp);
			}
			$logFile = file_get_contents($logFilePath);
			$logFile .= '.';
			return file_put_contents($logFilePath, $logFile);
		}

	}