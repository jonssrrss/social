var messager = {
    send: function(message, id) {
        console.log(id);
        if (message != '') {
            this.addMessage('Вы', message, true);
            $.post(
                'bot.php',
                {
                    message: message,
                    id: id
                },
                function(resp){
                    console.log(resp);
                    messager.addMessage('bot', JSON.parse(resp)['message'], false);
                }
            );
        }
    },

    addMessage: function(name, message, me) {
        if (me == true) {
            var html = '<div class="message-item me">';
        } else {
            var html = '<div class="message-item">';
        }
        html +=
            '   <div class="name">' + name + '</div>' +
            '   <div class="message-content">' + message + '</div>' +
            '</div>'
        ;
        html +
        $('.messages').append(html);
        $(".messages").scrollTop($(".messages").prop('scrollHeight'));
    }
}