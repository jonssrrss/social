-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 03 2021 г., 21:20
-- Версия сервера: 10.3.22-MariaDB
-- Версия PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `jxontaxi`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `order_success` int(11) NOT NULL COMMENT 'завершен ли заказ? 0 - нет, 1 -да',
  `order_active` int(11) NOT NULL COMMENT '0 - нет\r\n1 - да'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`order_id`, `user_id`, `order_success`, `order_active`) VALUES
(2, 4, 0, 0),
(3, 8, 0, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `order_list`
--

CREATE TABLE `order_list` (
  `list_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `list_address_name` text NOT NULL,
  `list_address_lat` text NOT NULL,
  `list_address_lon` text NOT NULL,
  `list_type` int(11) NOT NULL COMMENT 'тип точки (1 - начальная точка, 100 - конечная)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order_list`
--

INSERT INTO `order_list` (`list_id`, `order_id`, `list_address_name`, `list_address_lat`, `list_address_lon`, `list_type`) VALUES
(65, 2, 'Респ Башкортостан, г Уфа, ул Менделеева, 213/2', '54.74581', '56.028122', 1),
(66, 2, 'Респ Башкортостан, г Уфа, ул Степана Кувыкина, 17/1', '54.702415', '55.994236', 100),
(67, 3, 'Респ Башкортостан, г Уфа, ул Степана Кувыкина, 17/1', '54.702415', '55.994236', 1),
(68, 3, 'Респ Башкортостан, г Уфа, ул Академика Королева, 27', '54.7727999', '56.0802147', 100);

-- --------------------------------------------------------

--
-- Структура таблицы `sendorderstotaxi`
--

CREATE TABLE `sendorderstotaxi` (
  `send_id` int(11) NOT NULL,
  `send_time` int(11) NOT NULL,
  `id_taxi` int(11) NOT NULL,
  `id_order` int(11) NOT NULL,
  `send_response` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sendorderstotaxi`
--

INSERT INTO `sendorderstotaxi` (`send_id`, `send_time`, `id_taxi`, `id_order`, `send_response`) VALUES
(1, 1610401768, 9, 3, 0),
(2, 1610402030, 9, 3, 0),
(3, 1610402035, 9, 3, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `taxi`
--

CREATE TABLE `taxi` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `auto_model` text NOT NULL,
  `auto_mark` text NOT NULL,
  `auto_color` text NOT NULL,
  `auto_numbers` text NOT NULL,
  `confirmed` int(11) NOT NULL,
  `active` int(11) NOT NULL,
  `lat` double NOT NULL,
  `lon` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `taxi`
--

INSERT INTO `taxi` (`id`, `user_id`, `auto_model`, `auto_mark`, `auto_color`, `auto_numbers`, `confirmed`, `active`, `lat`, `lon`) VALUES
(4, 4, 'bmw', 'lada', 'белый', 'Т474ЕО102', 1, 0, 0, 0),
(5, 1, '21099', 'lada', 'Серебристый', 'Т474ЕО102', 1, 0, 0, 0),
(6, 5, '21099', 'lada', 'Серебристый', 'Т474ЕО111', 1, 0, 0, 0),
(7, 8, '21099', 'Лада', 'Серебристый', 'Т474ЕО102', 1, 0, 0, 0),
(8, 9, 'e39', 'BMW', 'Черный', 'А111АА222', 1, 1, 54.702415, 55.994236);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `user_name` text NOT NULL,
  `user_phone` text NOT NULL,
  `user_tg_id` int(11) NOT NULL,
  `user_nick` text NOT NULL,
  `user_status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_phone`, `user_tg_id`, `user_nick`, `user_status`) VALUES
(1, 'Евгений', '89173597343', 1, '', 3),
(2, 'евгений', '89173597343', 232, '', 3),
(3, 'john', '89173597343', 132, '', 4),
(4, 'John', '89173597343', 2, '', 3),
(5, 'Евген', '8917', 3, '', 25),
(6, 'Евгений', '89173597343', 5, '', 8),
(7, 'dwad', '132132', 123, '', 31),
(8, 'Тестовый', '89173597343', 122, '', 24),
(9, 'Евгений', '89173597343', 23, '', 32);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`);

--
-- Индексы таблицы `order_list`
--
ALTER TABLE `order_list`
  ADD PRIMARY KEY (`list_id`);

--
-- Индексы таблицы `sendorderstotaxi`
--
ALTER TABLE `sendorderstotaxi`
  ADD PRIMARY KEY (`send_id`);

--
-- Индексы таблицы `taxi`
--
ALTER TABLE `taxi`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `order_list`
--
ALTER TABLE `order_list`
  MODIFY `list_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT для таблицы `sendorderstotaxi`
--
ALTER TABLE `sendorderstotaxi`
  MODIFY `send_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT для таблицы `taxi`
--
ALTER TABLE `taxi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
