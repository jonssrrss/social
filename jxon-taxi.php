<?
	class JxonTaxi {

		// Новый массив с вопросами
		public $responses = array();

		public $id_tg; // id телеграма
		public $id_base; // id в базе данных
		public $name; // имя пользователя
		public $phone; // телефон пользователя
		public $db; // компонент базы данных

		public $dadataToken = "3c38818f1eddd8fdc0b44a65f72206f01af46adad8";
		public $dadataSecret = "81d8e11321c941c3d92c077wfaee55a5e75sadde81";

		function __construct($id_tg) {
			$this->id_tg = $id_tg;
			$this->responses = require_once('responses.php');
			if (!isset($_SESSION['db'])) {
				$this->db = new DB();
				$_SESSION['db'] = $this->db;
			} else {
				$this->db = $_SESSION['db'];
			}
		}

		/**
		 * Логирование
		 */
		function logPr($text) {
			$logFilePath = 'logi.txt';
			if (is_array($text)) {
				$text = json_encode($text);
			}
			if (!file_exists($logFilePath)) {
				$fp = fopen($logFilePath, "w");
				fclose($fp);
			}
			$logFile = file_get_contents($logFilePath);
			$logFile .= date("Y-m-d H:i:s").": ".$text."\n\n";
			return file_put_contents($logFilePath, $logFile);
		}

		/**
		 * Обработка и идентификация пользователя
		 * @message - входящее сообщение
		 */
		function init($message) {
			// Сначала ищем пользователя в базе по id
			if ($this->id_base = $this->getUserIdBase($this->id_tg)) {
				// Если находим - смотрим, указано ли у него имя
				if ($this->getName($this->id_base)) {
					// Если находим - смотрим, указано ли у него номер телефона
					if ($this->getPhone($this->id_base)) {
						// Если человек захотел стать водителем
						if (($this->getUserStatus($this->id_base) == '3') && ($message == 'Стать водителем')) {
							// Смотрим, есть ли он в таблице таксистов
							if ($this->isUserTaxi($this->id_base)) {
								// Смотрим, указал ли он марку авто
								if ($this->isSetMarkTaxi($this->id_base)) {
									// Смотрим, указал ли он модель авто
									if ($this->isSetModelTaxi($this->id_base)) {
										// Смотрим, указал ли он цвет авто
										if ($this->isSetColorTaxi($this->id_base)) {
											// Смотрим, указал ли он номера на авто
											if ($this->isSetNumbersTaxi($this->id_base)) {
												// return array(
												// 	'message' => 'Сделать проверку на подтверждение таксиста'
												// );
												return $this->command('?', $message);
											} else {
												return $this->command(23, $message);
											}
										} else {
											return $this->command(22, $message);
										}
									} else {
										return $this->command(21, $message);
									}
								} else {
									return $this->command(20, $message);
								}
							} else {
								// Создаем таксиста
								if ($this->createTaxist($this->id_base)) {
									// Заново вызываем обработку команд
									return $this->init($message);
								}
							}
						// Остальные команды
						} else {
							return $this->command('?', $message);
						}
					// Если телефон не указан - спрашиваем телефон
					} else {
						return $this->command(2, $message);
					}
				// Если имя не установлено - спрашиваем имя
				} else {
					return $this->command(1, $message);
				}
			// Если не находим - создаем у нас в базе связку
			} else {
				if ($this->id_base = $this->createNewUser()) {
					// Заново вызываем обработку команд
					return $this->init($message);
				}
			}
		}

		/**
		 * Обработка команды
		 * @command - идентификатор команды
		 * @message - входящее сообщение
		 **/
		function command($command, $message) {
			if ($command != '?') {
				if ($status = $this->getUserStatus($this->id_base)) {

					if ($status != $command) {
						if ($this->updateUserStatus($this->id_base, $command)) {
							if (isset($this->responses[$command]['msg']['text'])) {
								return array(
									'message' => $this->responses[$command]['msg']['text'].$this->getCommands($command)['txt'],
									'btn' => $this->getCommands($command)['btn']
								);
							} else {
								if (isset($this->responses[$command]['msg']['func'])) {
									$f = '$res = $this->'.$this->responses[$command]['msg']['func'].'();';
									eval($f);
									return $res;
								}
							}
						}
					} else {
						// Смотрим, ожидает ли шаг ответы
						if (isset($this->responses[$command]['responses'])) {
							// Если да - ищем ответ пользователя среди вариантов ответов
							if (isset($this->responses[$command]['responses'][$message])) {
								// Если есть - смотрим, функция ли должна выполниться (либо просто следующий шаг)
								if (isset($this->responses[$command]['responses'][$message]['func'])) {
									
									if ($this->updateUserStatus($this->id_base, $this->responses[$command]['responses'][$message]['func']['next'])) {
										// 
										if (isset(
											$this->responses[
												$this->responses[$command]['responses'][$message]['func']['next']
											]['msg']['func']
										)) {
											$f = '$res = $this->'.$this->responses[$command]['msg']['func'].'();';
											eval($f);
											return $res;
										} else {
											$f = '$res = $this->'.$this->responses[$command]['responses'][$message]['func']['name'].'();';
											eval($f);
											if ($res == true) {
												return array(
													// 'message' => $this->responses[$command]['msg']['text']
													'message' => $this->responses[$this->responses[$command]['responses'][$message]['func']['next']]['msg']['text'].$this->getCommands($this->responses[$command]['responses'][$message]['func']['next'])['txt'],
													'btn' => $this->getCommands($this->responses[$command]['responses'][$message]['func']['next'])['btn']
												);
											} else {
												return array(
													// 'message' => $this->responses[$command]['msg']['text']
													'message' => 'тут может что-то пойти не так',
													'btn' => $this->getCommands($command)['btn']
												);
											}
										}

									}
								} else {
									return $this->command($this->responses[$command]['responses'][$message]['next'], $message);
								}
							} else {
								if (isset($this->responses[$command]['func']['name'])) {
									$f = '$res = $this->'.$this->responses[$command]['func']['name'].'(\''.$message.'\');';
									eval($f);
								} else {
									return array(
										'message' => $this->responses[$command]['error']['text'].$this->getCommands($command)['txt'],
										'btn' => $this->getCommands($command)['btn']
									);
								}


								if ($res == true) {
									if (isset($this->responses[$this->responses[$command]['func']['next']]['msg']['text'])) {
										if ($this->updateUserStatus($this->id_base, $this->responses[$command]['func']['next'])) {
											return array(
												'message' => $this->responses[$this->responses[$command]['func']['next']]['msg']['text'].$this->getCommands($this->responses[$command]['func']['next'])['txt'],
												'btn' => $this->getCommands($this->responses[$command]['func']['next'])['btn']
											);
										}
									} elseif (isset($this->responses[$this->responses[$command]['func']['next']]['msg']['func'])) {

										if ($this->updateUserStatus($this->id_base, $this->responses[$command]['func']['next'])) {
											$f = '$res = $this->'.$this->responses[$this->responses[$command]['func']['next']]['msg']['func'].'();';
											eval($f);
											return $res;
										}
									}
								} else {
									if (isset($this->responses[$command]['error']['text'])) {
										return array(
											'message' => $this->responses[$command]['error']['text'].$this->getCommands($command)['txt'],
											'btn' => $this->getCommands($command)['btn']
										);
									}
								}
							}



						} elseif (isset($this->responses[$command]['func'])) {
							
							eval('$res = $this->'.$this->responses[$command]['func']['name'].'(\''.$message.'\');');
							if ($res == true) {
								if (isset($this->responses[$this->responses[$command]['func']['next']]['msg']['text'])) {
									if ($this->updateUserStatus($this->id_base, $this->responses[$command]['func']['next'])) {
										return array(
											'message' => $this->responses[$this->responses[$command]['func']['next']]['msg']['text'].$this->getCommands($this->responses[$command]['func']['next'])['txt'],
											'btn' => $this->getCommands($this->responses[$command]['func']['next'])['btn']
										);
									}
								}
							} else {
								if (isset($this->responses[$command]['error']['text'])) {
									return array(
										'message' => $this->responses[$command]['error']['text'].$this->getCommands($command)['txt'],
										'btn' => $this->getCommands($command)['btn']
									);
								}
							}
						}
					}

				}
			} else {
				if ($status = $this->getUserStatus($this->id_base)) {
					return $this->command($status, $message);
				}
			}
		}

		/**
		 * Формирование кнопок из массива responses
		 * @command - номер элемента из массива responses
		 */
		function getCommands($command) {
			if (isset($this->responses[$command]['responses'])) {
				$msg = '';
				$btn = array();
				foreach ($this->responses[$command]['responses'] as $key => $value) {
					$msg .= PHP_EOL.$key.' - '.$value['name'];
					// $btn[$key] = $value['name'];
					array_push($btn, array(
						// 'name' => $value['name'],
						'name' => $key,
						'num' => $key
					));
				}
				return array(
					// 'txt' => $msg,
					'txt' => '',
					'btn' => $btn
				);
			} else {
				$btn = array();
				return array(
					'txt' => '',
					'btn' => $btn
				);
			}
		}

		/**
		 * Формирование заготовленных командой getCommands кнопок
		 * @btn - массив кнопок
		 */
		function generateButtons($btn) {
			$keyboard = array(
				array()
			);
			

			// НУЖНО ВЫРАВНИТЬ КЛАВУ
			$i = 0;
			foreach ($btn as $key => $value) {
				if (count($keyboard[$i]) == 2) {
					$i++;
					array_push($keyboard,
						array()
					);
				}
				if ($value['name'] == 'loc') {
					array_push($keyboard[$i],
						array(
							'text' => 'Отправить свое местоположение',
							'request_location' => true,
						)
					);
				} elseif ($value['name'] == 'con') {
					array_push($keyboard[$i],
						array(
							'text' => 'Отправить свой номер',
							'request_contact' => true,
						)
					);
				} else {
					array_push($keyboard[$i],
						array(
							'text' => $value['name'],
							'callback_data' => $value['name']
						)
					);
				}
			}
			// END НУЖНО ВЫРАВНИТЬ КЛАВУ
			$inlineKeyboardMarkup = array(
				'keyboard' => $keyboard,
				'resize_keyboard' => true,
			);
			return json_encode($inlineKeyboardMarkup);
		}

		/**
		 * Поиск клиентов для таксиста
		 * @numbers - номера
		 **/
		function findOrders($message) {
			return array(
				'message' => 'Ищу для тебя клиентов (написать функцию)'
			);
		}

		/**
		 * Установка номеров авто
		 * @numbers - номера
		 **/
		function updateNumbers($numbers) {
			$query = "
				UPDATE
					`taxi`
				SET
					`auto_numbers` = '{$numbers}'
				WHERE
					(user_id = {$this->id_base})
			";
			$result = $this->db->select($query);
			return true;
		}

		/**
		 * Установка цвета авто
		 * @color - цвет
		 **/
		function updateColor($color) {
			$query = "
				UPDATE
					`taxi`
				SET
					`auto_color` = '{$color}'
				WHERE
					(user_id = {$this->id_base})
			";
			$result = $this->db->select($query);
			return true;
		}

		/**
		 * Установка марки авто
		 * @mark - марка
		 **/
		function updateMark($mark) {
			$query = "
				UPDATE
					`taxi`
				SET
					`auto_mark` = '{$mark}'
				WHERE
					(user_id = {$this->id_base})
			";
			$result = $this->db->select($query);
			return true;
		}

		/**
		 * Установка модели авто
		 * @model - модель
		 **/
		function updateModel($model) {
			$query = "
				UPDATE
					`taxi`
				SET
					`auto_model` = '{$model}'
				WHERE
					(user_id = {$this->id_base})
			";
			$result = $this->db->select($query);
			return true;
		}

		/**
		 * Является ли пользователь таксистом
		 * @id_base - id пользователя
		 */
		function isUserTaxi($id_base) {
			if ($this->getTaxiById($id_base)) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Получить инфо о таксисте
		 * @id_base - id пользователя
		 */
		function getTaxiById($id_base) {
			$query = "
				SELECT
					*
				FROM
					`users`,
					`taxi`
				WHERE
					(taxi.user_id = users.user_id) AND
					(taxi.user_id = {$id_base})
			";
			$result = $this->db->select($query);
			if (count($result) == 1) {
				return $result[0];
			} else {
				return false;
			}
		}

		
		/**
		 * Указаны ли номера авто
		 * @id_base - id пользователя
		 */
		function isSetNumbersTaxi($id_base) {
			if ($taxi = $this->getTaxiById($this->id_base)) {
				if ($taxi['auto_numbers'] != '') {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * Указан ли цвет авто
		 * @id_base - id пользователя
		 */
		function isSetColorTaxi($id_base) {
			if ($taxi = $this->getTaxiById($this->id_base)) {
				if ($taxi['auto_color'] != '') {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Указана ли марка такси
		 * @id_base - id пользователя
		 */
		function isSetMarkTaxi($id_base) {
			if ($taxi = $this->getTaxiById($this->id_base)) {
				if ($taxi['auto_mark'] != '') {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Указана ли модель такси
		 * @id_base - id пользователя
		 */
		function isSetModelTaxi($id_base) {
			if ($taxi = $this->getTaxiById($this->id_base)) {
				if ($taxi['auto_model'] != '') {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Сделать из пользователя таксиста
		 * @id_base - id пользователя
		 */
		function createTaxist($id_base) {
			$query = "
				INSERT INTO
					`taxi`
				(
					`id`,
					`user_id`,
					`auto_model`,
					`auto_mark`,
					`auto_color`,
					`auto_numbers`,
					`confirmed`,
					`active`,
					`lat`,
					`lon`
				)
				VALUES
				(
					NULL,
					'{$id_base}',
					'',
					'',
					'',
					'',
					'1',
					'0',
					'0',
					'0'
				)
			";
			$result = $this->db->insert($query);
			return $result;
		}

		/**
		 * Таксист готов к работе
		 * @id_base - id пользователя
		 */
		function activeTaxist($id_base) {
			$query = "
				UPDATE
					`taxi`
				SET
					`active` = '1'
				WHERE
					`taxi`.`user_id` = {$id_base};
			";
			$this->db->select($query);
			return true;
		}

		/**
		 * Таксист закончил работу
		 */
		function deactiveTaxist() {
			if ($this->rejectAllOrders()) {
				$query = "
					UPDATE
						`taxi`
					SET
						`active` = '0'
					WHERE
						`taxi`.`user_id` = {$this->id_base};
					
					DELETE FROM
						`sendOrdersToTaxi`
					WHERE
						`id_taxi` = {$this->id_base} AND
						(
							`send_response` = 0 OR
							`send_response` = 1
						)
				";
				$this->db->select($query);
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Установка начального адреса
		 * @message - адрес
		 */
		function setPointA($message) {
			if ($order = $this->getStartingOrder($this->id_base)) {
				return $this->addAddress($order, $message, false);
			} else {
				if ($this->createNewOrder($this->id_base)) {
					return $this->setPointA($message);
				}
			}
		}

		/**
		 * Установка конечного адреса
		 * @message - адрес
		 */
		function setPointB($message) {
			if ($order = $this->getStartingOrder($this->id_base)) {
				return $this->addAddress($order, $message, true);
			} else {
				return false;
			}
		}

		/**
		 * Добавление адреса в заказ
		 * @order - id заказа
		 * @address - введенный пользователем адрес
		 * @pointType - если true - конечная точка
		 */
		function addAddress($order, $address, $pointType = true) {
			if ($resultAddress = $this->getGeo($address)) {
				if ($pointType == false) {
					$pointType = 1;
				} else {
					$pointType = 100;
				}

				$addressName = $resultAddress['region_with_type'];
				if (isset($resultAddress['city_with_type'])) {
					$addressName .= ', '.$resultAddress['city_with_type'];
				}
				if (isset($resultAddress['settlement_with_type'])) {
					$addressName .= ', '.$resultAddress['settlement_with_type'];
				}
				if (isset($resultAddress['street_with_type'])) {
					$addressName .= ', '.$resultAddress['street_with_type'];
				}
				if (isset($resultAddress['house'])) {
					$addressName .= ', '.$resultAddress['house'];
				}

				if ($pointType == 1) {
					$query = "
						DELETE FROM
							`order_list`
						WHERE
							`order_list`.`order_id` = {$order};
					";
					$this->db->select($query);
				} else {
					$query = "
						DELETE FROM
							`order_list`
						WHERE
							`order_list`.`order_id` = {$order} AND
							`order_list`.`list_type` = 100
					";
					$this->db->select($query);
				}

				$query = "
					INSERT INTO
						`order_list`
					(
						`list_id`,
						`order_id`,
						`list_address_name`,
						`list_address_lat`,
						`list_address_lon`,
						`list_type`
					)
					VALUES
					(
						NULL,
						'{$order}',
						'{$addressName}',
						'{$resultAddress['geo_lat']}',
						'{$resultAddress['geo_lon']}',
						'{$pointType}'
					)
				";
				if ($this->db->insert($query)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Активация заказа
		 */
		function activateOrder() {
			if ($order = $this->getStartingOrder($this->id_base)) {
				$query = "
					UPDATE
						`orders`
					SET
						`order_active` = '1'
					WHERE
						`orders`.`order_id` = {$order};
				";
				$this->db->select($query);
				// Нужно найти водителя и отправить ему этот заказ
				if ($taxi_user_id = $this->findTaxiToClient()) {



					if ($this->sendOrderToTaxi($order, $taxi_user_id)) {





						// $method = 'sendMessage';
						// $send_data = [
						// 	'chat_id' => '1573720701',
						// 	'text' => 'log:'.PHP_EOL.'Да, отметили в базе заказ таксисту с id: '.$taxi_user_id.' и order - '.$order
						// ];
						// $this->sendTelegram($method, $send_data, true);








						if ($orderDesc = $this->getDescOrder($order)) {
							


							// $method = 'sendMessage';
							// $send_data = [
							// 	'chat_id' => '1573720701',
							// 	'text' => $orderDesc
							// ];
							// $this->sendTelegram($method, $send_data, true);



							if ($taxi_user_tg_id = $this->getUser($taxi_user_id)['user_tg_id']) {

							// 	if ($this->updateUserStatus($taxi_user_id, 32)) {


									$method = 'sendMessage';
									$send_data = [
										'chat_id' => $taxi_user_tg_id,
										'text' => $orderDesc,
										'reply_markup' => $this->generateButtons(
											$this->getCommands(32)['btn']
										),
										'parse_mode' => 'Markdown',
									];
									$this->sendTelegram($method, $send_data, true);
	
	
							// 	}
							}
						}
					}
				}
				// end
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Деактивация заказа
		 * @order - id заказа
		 */
		function deActivateOrder() {
			if ($order = $this->getStartingOrder($this->id_base)) {
				$query = "
					DELETE FROM
						`orders`
					WHERE
						`orders`.`order_id` = {$order};

					DELETE FROM
						`order_list`
					WHERE
						`order_list`.`order_id` = {$order}
				";
				$this->db->select($query);
				return true;
			} else {
				return true;
			}
		}

		/**
		 * Получить инфо о последнем заказе
		 */
		function getInfoLastOrder() {
			if ($order = $this->getStartingOrder($this->id_base)) {
				return array(
					'message' => $this->getInfoOrder($order).PHP_EOL.PHP_EOL.'**Создаем заявку?**'.$this->getCommands(6)['txt'],
					'btn' => $this->getCommands(6)['btn']
				);
			} else {
				return false;
			}
		}


		/**
		 * Получить инфо о профиле
		 */
		function getProfileInfo() {
			if ($profile = $this->getUser($this->id_base)) {
				return array(
					// 'message' => $this->getInfoOrder($order).PHP_EOL.'Создаем заявку?'.$this->getCommands(6)['txt'],
					'message' =>
						'Имя: '.$profile['user_name'].PHP_EOL.
						'Телефон: '.$profile['user_phone'],
					'btn' => $this->getCommands(8)['btn']
				);
			} else {
				return false;
			}
		}


		/**
		 * Получить инфо о профиле таксиста
		 */
		function getAutoInfo() {
			if ($auto = $this->getTaxiById($this->id_base)) {
				return array(
					// 'message' => $this->getInfoOrder($order).PHP_EOL.'Создаем заявку?'.$this->getCommands(6)['txt'],
					'message' =>
						'Марка: '.$auto['auto_mark'].PHP_EOL.
						'Модель: '.$auto['auto_model'].PHP_EOL.
						'Цвет: '.$auto['auto_color'].PHP_EOL.
						'Номера: '.$auto['auto_numbers'],
					'btn' => $this->getCommands(26)['btn']
				);
			} else {
				return false;
			}
		}


		/**
		 * Получение информации о заказе в виде сообщения для пользователя/водителя
		 * @order - id заказа
		 */
		function getInfoOrder($order) {
			$orderResult = $this->getAddresses($order);
			$infoRoute = $this->getInfoRoute(
				$orderResult[0]['list_address_lat'],
				$orderResult[0]['list_address_lon'],
				$orderResult[1]['list_address_lat'],
				$orderResult[1]['list_address_lon']
			);
			$distance = round($infoRoute['routes'][0]['distance']/1000, 3);
			$minutes = round($infoRoute['routes'][0]['duration']/60, 1);
			// $priceKM = 3.8;
			$priceKM = 8;
			$price = ceil(($distance*$priceKM)*(1.08/($distance/$minutes))+50);

			$info =
				'`Начальный адрес:`'.PHP_EOL."*".$orderResult[0]['list_address_name']."*".PHP_EOL.PHP_EOL.
				'`Конечный адрес:`'."*".PHP_EOL.$orderResult[1]['list_address_name']."*".PHP_EOL.PHP_EOL.
				'`Расстояние: `'."*".$distance.' км'."*".PHP_EOL.
				'`Время в пути: `'."*".$minutes.' минут'."*".PHP_EOL.PHP_EOL.
				'`Стоимость поездки: `'."*".$price.' руб.'."*".PHP_EOL.
				'`Способ оплаты:` *наличные*'
			;
			return $info;
		}


		/**
		 * Получение информации о маршруте
		 * @a_lat - широта начальной точки
		 * @a_lon - долгота начальной точки
		 * @b_lat - широта конечной точки
		 * @b_lon - долгота конечной точки
		 */
		function getInfoRoute($a_lat, $a_lon, $b_lat, $b_lon) {
			$url = "https://routing.openstreetmap.de/routed-car/route/v1/driving/{$a_lon},{$a_lat};{$b_lon},{$b_lat}";
			$response = file_get_contents($url);
			return json_decode($response, true);
		}

		/**
		 * Получение списка адресов из заказа
		 * @order - id заказа
		 */
		function getAddresses($order) {
			$query = "
				SELECT
					*
				FROM
					`order_list`
				WHERE
					`order_id` = {$order}
			";
			return $this->db->select($query);
		}

		/**
		 * проверка на существование адресов в заказе
		 * @order - id заказа
		 */
		function issetAddresses($order) {
			if (count($this->getAddresses($order))>0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Получение информации о начальном адресе
		 * @adress - адрес
		 */
		function setGeoA($adress) {
			if ($geo = $this->getGeo($adress)) {
				return array(
					'message' =>'Начальная точка:'.PHP_EOL.
					$geo['region_with_type'].', '.
					$geo['city_with_type'].', '.
					$geo['settlement_with_type'].', '.
					$geo['street_with_type'].', '.
					$geo['house'].PHP_EOL.
					'Куда поедем?'
				);
				return array(
					'message' => $geo
				);
			} else {
				return array(
					'message' => 'Этот адрес неизвестен, попробуй написать его по - другому'
				);
			}
		}

		/**
		 * Получение информации о конечном адресе
		 * @adress - адрес
		 */
		function setGeoB($adress) {
			if ($geo = $this->getGeo($adress)) {
				return array(
					'message' =>'Конечная точка:'.PHP_EOL.
					$geo['region_with_type'].', '.
					$geo['city_with_type'].', '.
					$geo['settlement_with_type'].', '.
					$geo['street_with_type'].', '.
					$geo['house'].PHP_EOL.
					'Все верно или изменить заказ?'
				);
				return array(
					'message' => $geo
				);
			} else {
				return array(
					'message' => 'Этот адрес неизвестен, попробуй написать его по - другому'
				);
			}
		}

		/**
		 * Получение полного адреса из набора в строке
		 * @adress - введенный пользователем адрес
		 */
		function getGeo($adress) {
			require($_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php');
			$dadata = new \Dadata\DadataClient($this->dadataToken, $this->dadataSecret);
			$result = $dadata->suggest("address", $adress)[0]['data'];
			if (isset($result['geo_lat'])) {
				return $result;
			} else {
				return false;
			}
		}


		/**
		 * Установка имени пользователя
		 * @name - имя
		 **/
		function updateName($name) {
			if (preg_match('/^[a-zA-Zа-яёА-ЯЁ]+$/u', $name)) {
				$query = "
					UPDATE
						`users`
					SET
						`user_name` = '{$name}'
					WHERE
						(user_id = {$this->id_base})
				";
				$result = $this->db->select($query);
				$this->name = $name;
				return true;
			} else {
				return false;
			}
		}


		/**
		 * Установка номера телефона пользователя
		 * @phone - номер телефона
		 **/
		function updatePhone($phone) {
			if (preg_match('/^((8|\+7)[\- ]?)?(\(?\d{9,11}\)?[\- ]?)?[\d\- ]{9,11}$/', $phone)) {
				$query = "
					UPDATE
						`users`
					SET
						`user_phone` = '{$phone}'
					WHERE
						(user_tg_id = {$this->id_tg})
				";
				$result = $this->db->select($query);
				$this->phone = $phone;
				return true;
			} else {
				return false;
			}
		}


		/**
		 * Установка статуса пользователя
		 * @id_base - id в базе
		 * @command - номер статуса
		 **/
		function updateUserStatus($id_base, $command) {
			$query = "
				UPDATE
					`users`
				SET
					`user_status` = '{$command}'
				WHERE
					(user_id = {$id_base})
			";
			$result = $this->db->select($query);
			return true;
		}


		/**
		 * Получение статуса пользователя
		 * @id_base - id в базе
		 **/
		function getUserStatus($id_base) {
			if ($result = $this->getUser($id_base)) {
				if ($result['user_status'] != '') {
					// echo 'USER_STATUS='.$result['user_status'];
					return $result['user_status'];
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * Получение id юзера
		 * @id_tg - id в telegram
		 **/
		function getUserIdBase($id_tg) {
			$query = "
				SELECT
					*
				FROM
					users
				WHERE
					(user_tg_id = ".$id_tg.")
			";
			$result = $this->db->select($query);
			if (count($result) != 1) {
				return false;
			} else {
				return $result[0]['user_id'];
			}
		}


		/**
		 * Создаем новоый заказ
		 * @id_base - id в базе
		 **/
		function createNewOrder($id_base) {
			$query = "
				INSERT INTO
					`orders`
				(
					`order_id`,
					`user_id`,
					`order_success`,
					`order_active`
				)
				VALUES
				(
					NULL,
					{$id_base},
					0,
					0
				)
			";
			$result = $this->db->insert($query);
			return $result;
		}


		/**
		 * Получение начатого заказа
		 * @id_base - id в базе
		 **/
		function getStartingOrder($id_base) {
			$query = "
				SELECT
					*
				FROM
					orders
				WHERE
					(user_id = ".$id_base.") AND
					(order_success = '0')
			";
			$result = $this->db->select($query);
			if (count($result) != 1) {
				return false;
			} else {
				return $result[0]['order_id'];
			}
		}


		/**
		 * Получение юзера
		 * @id_base - id в базе
		 **/
		function getUser($id_base) {
			$query = "
				SELECT
					*
				FROM
					users
				WHERE
					(user_id = ".$id_base.")
			";
			$result = $this->db->select($query);
			if (count($result) != 1) {
				return false;
			} else {
				return $result[0];
			}
		}


		/**
		 * Получение имени
		 * @id_base - id в базе
		 **/
		function getName($id_base) {
			if ($user = $this->getUser($id_base)) {
				if ($user['user_name'] != '') {
					return $user['user_name'];
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * Получение номера телефона
		 * @id_base - id в базе
		 **/
		function getPhone($id_base) {
			if ($user = $this->getUser($id_base)) {
				if ($user['user_phone'] != '') {
					return $user['user_phone'];
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * Создаем нового юзера
		 **/
		function createNewUser() {
			$query = "
				INSERT INTO
					`users`
				(
					`user_id`,
					`user_name`,
					`user_phone`,
					`user_tg_id`,
					`user_nick`,
					`user_status`
				)
				VALUES
				(
					NULL,
					'',
					'',
					'{$this->id_tg}',
					'',
					1000
				)
			";
			$result = $this->db->insert($query);
			return $result;
		}


		/**
		 * Обновить геопозицию у таксиста (дописать)
		 **/
		function updateMyGeo($geo) {
			if ($this->activeTaxist($this->id_base)) {
				if ($geo = explode(';', $geo)) {
					if (count($geo) == 2) {
						$query = "
							UPDATE
								`taxi`
							SET
								`lat` = '{$geo[0]}',
								`lon` = '{$geo[1]}'
							WHERE
								(user_id = {$this->id_base})
						";
						$this->db->select($query);
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}


		/**
		 * Выдать сообщение либо "ищу для тебя заявку", либо выдать краткое инфо о заявке
		 **/
		function getOrder() {
			if ($this->canselOrderTaxist()) {
				// Отклонить все заявки, на которые не ответил
				if ($this->rejectAllOrders()) {

					if ($order = $this->findOrderToTaxist()) {
						return array(
							'message' => 'Есть заявка:'.PHP_EOL.$order,
							'btn' => $this->getCommands(32)['btn']
						);
					} else {
						return array(
							'message' => 'Ищу для тебя заявку',
							'btn' => $this->getCommands(31)['btn']
						);
					}
				} else {
					return false;
				}
			}
		}

		/**
		 * То же, что и getOrder, только правильно завершаем текущий заказ перед этим
		 */
		function getNewOrder() {
			if ($order = $this->getLastSendOrderToTaxistYes()) {
				if ($this->successLastSendOrderToTaxistYes()) {
					$query = "
						UPDATE
							`orders`
						SET
							`order_success` = '1',
							`order_active` = '0'
						WHERE
							`order_id` = {$order}
					";
					$result = $this->db->select($query);
					return $this->getOrder();
				}
			} else {
				return false;
			}
		}

		/**
		 * Отклонить все заявки
		 */
		function rejectAllOrders() {
			$query = "
				UPDATE
					`sendOrdersToTaxi`
				SET
					`send_response` = '1'
				WHERE
					(id_taxi = {$this->id_base}) AND
					(`send_response` = '0')
			";
			$result = $this->db->select($query);
			return true;
		}

		/**
		 * Поиск такси для клиента
		 */
		function findTaxiToClient() {
			// у нас есть только id клиента. нужно сначала вытащить его текущую заявку
			if ($order = $this->getStartingOrder($this->id_base)) {
				// получаем адреса из id заказа
				if ($orderResult = $this->getAddresses($order)) {
					/**
					 * ищем рядом ожидающего таксиста
					 * 
					 * $orderResult[0]['list_address_lat'],
					 * $orderResult[0]['list_address_lon'],
					 * $orderResult[1]['list_address_lat'],
					 * $orderResult[1]['list_address_lon']
					 */
					$const_km = 0.015626;
					$dist = $const_km*2;

					$lat_max = $orderResult[0]['list_address_lat'] + $dist;
					$lat_min = $orderResult[0]['list_address_lat'] - $dist;
					$lon_max = $orderResult[0]['list_address_lon'] + $dist;
					$lon_min = $orderResult[0]['list_address_lon'] - $dist;
					$query = "
						SELECT
							*
						FROM
							taxi
							LEFT JOIN sendOrdersToTaxi ON sendOrdersToTaxi.id_taxi = taxi.id
						WHERE
							taxi.active = 1 AND
							(
								sendOrdersToTaxi.send_response IS NULL
								OR sendOrdersToTaxi.send_response = 1
							) AND
							taxi.lat < {$lat_max} AND
							taxi.lat > {$lat_min} AND 
							taxi.lon < {$lon_max} AND
							taxi.lon > {$lon_min} 
					";
					if ($result = $this->db->select($query)) {
						if (count($result) > 0) {






							return $result[0]['user_id'];
						} else {
							return false;
						}
					} else {
						return false;
					}
				} else {
					return false;
				}
			} else {
				return false;
			}
		}

		/**
		 * Поиск заявки для таксиста
		 * @not - поиск среди отклоненных
		 */
		function findOrderToTaxist($not = false) {
			if ($taxi = $this->getTaxiById($this->id_base)) {
				$const_km = 0.015626;
				$dist = $const_km*2;

				$lat_min = $taxi['lat'] - $dist;
				$lat_max = $taxi['lat'] + $dist;
				$lon_min = $taxi['lon'] - $dist;
				$lon_max = $taxi['lon'] + $dist;

				$query = "
					SELECT
						*
					FROM
						orders
						LEFT JOIN order_list ON orders.order_id = order_list.order_id
						LEFT JOIN sendOrdersToTaxi ON sendOrdersToTaxi.id_order = orders.order_id
					WHERE
						list_type = 1 AND

						list_address_lat > {$lat_min} AND
						list_address_lat < {$lat_max} AND
						list_address_lon > {$lon_min} AND
						list_address_lon < {$lon_max} AND

						order_success = 0
						AND order_active = 1
						AND (
							-- send_time < '".(time() - 15)."' OR (send_time IS NULL OR send_response = 1)
							send_time < '".(time() - 15)."' OR send_response IS NULL OR send_response = 1
						)
				";

				if ($not == false) {
					$query .="
						AND (sendOrdersToTaxi.send_response IS NULL -- OR sendOrdersToTaxi.send_response = 1
						)
					";
				} else {
					$query .="
						-- AND (sendOrdersToTaxi.send_response = 1
						-- )
					";
				}



				$res = $this->db->select($query);
				if (count($res) == 1) {
					if ($this->sendOrderToTaxi($res[0]['order_id'], $this->id_base)) {
						return $this->getDescOrder($res[0]['order_id']);
					}
				} elseif (count($res) == 0) {
					if ($not == true) {
						return false;
					} else {
						return $this->findOrderToTaxist(true);
					}
				} else {



					// Складываем все разницы в координатах
					foreach ($res as $key => $value) {
						// $taxi['lat']
						// $taxi['lon']
						$res[$key]['dist_lat'] = (float)$res[$key]['list_address_lat'] - (float)$taxi['lat'];
						if ((float)$res[$key]['dist_lat'] < 0) {
							$res[$key]['dist_lat'] = (float)$res[$key]['dist_lat'] * -1;
						}
						$res[$key]['dist_lon'] = (float)$res[$key]['list_address_lon'] - (float)$taxi['lon'];
						if ((float)$res[$key]['dist_lon'] < 0) {
							$res[$key]['dist_lon'] = (float)$res[$key]['dist_lon'] * -1;
						}
						$res[$key]['dist'] = (float)$res[$key]['dist_lat'] + (float)$res[$key]['dist_lon'];
					}


					// Найти самую ближайшую точку
					// $minDist = 100;
					// // $minIdOrd = 0;
					$minIdOrd = array();
					foreach ($res as $key => $value) {
						// if ((float)$res[$key]['dist'] < (float)$minDist) {
						// 	$minDist = (float)$res[$key]['dist'];
						// 	$minIdOrd[$res[$key]['dist']] = $res[$key]['order_id'];
						// }
						$minIdOrd[$res[$key]['order_id']] = $res[$key]['dist'];
					}

					asort($minIdOrd);

					// if ($this->sendOrderToTaxi($res[0]['order_id'], $this->id_base)) {
					// 	return $this->getDescOrder($res[0]['order_id']);
					// }
					foreach ($minIdOrd as $key => $value) {
						if ($this->sendOrderToTaxi($key, $this->id_base)) {
							return $this->getDescOrder($key);
						}
					}
				}
			} else {
				return false;
			}
		}

		/**
		 * Краткое описание таксисту о заказе
		 * @order - id заказа
		 */
		function getDescOrder($order) {
			$orderResult = $this->getAddresses($order);
			$infoRoute = $this->getInfoRoute(
				$orderResult[0]['list_address_lat'],
				$orderResult[0]['list_address_lon'],
				$orderResult[1]['list_address_lat'],
				$orderResult[1]['list_address_lon']
			);
			$distance = round($infoRoute['routes'][0]['distance']/1000, 3);
			$minutes = round($infoRoute['routes'][0]['duration']/60, 1);
			// $priceKM = 3.8;
			$priceKM = 8;
			$price = ceil(($distance*$priceKM)*(1.08/($distance/$minutes))+50);
			if ($taxi = $this->getTaxiById($this->id_base)) {
				$infoRoute = $this->getInfoRoute(
					$taxi['lat'],
					$taxi['lon'],
					$orderResult[0]['list_address_lat'],
					$orderResult[0]['list_address_lon']
				);
				$distanceToClient = round($infoRoute['routes'][0]['distance']/1000, 3);
			}
			$info =
				'Заявка №'.$order.PHP_EOL.
				'Расстояние до клиента: '.$distanceToClient.' км'.PHP_EOL.
				'Конечный адрес:'.PHP_EOL.$orderResult[1]['list_address_name'].PHP_EOL.PHP_EOL.

				'Расстояние: '.$distance.' км'.PHP_EOL.
				'Время в пути: '.$minutes.' минут'.PHP_EOL.
				'Стоимость поездки: '.$price.' руб.'.PHP_EOL.
				'Способ оплаты: наличные'
			;
			return $info;
		}

		function getInfoOrderToTaxist($order) {
			// $orderResult = $this->getAddresses($order);
			// $orderResult = 


			// Получаем полную информацию о заказе и водителе
			$query = "
				SELECT
					*,
					orders.user_id AS 'order_user'
				FROM
					sendOrdersToTaxi
					LEFT JOIN orders ON orders.order_id = sendOrdersToTaxi.id_order
					LEFT JOIN order_list ON orders.order_id=order_list.order_id
					-- LEFT JOIN users ON users.user_id = sendOrdersToTaxi.id_taxi
					LEFT JOIN users ON users.user_id = orders.user_id
					-- LEFT JOIN taxi ON taxi.user_id = users.user_id
					
				WHERE
					`id_order` = {$order}
			";
			$orderResult = $this->db->select($query);




			if (count($orderResult) == 2) {
				$infoRoute = $this->getInfoRoute(
					$orderResult[0]['list_address_lat'],
					$orderResult[0]['list_address_lon'],
					$orderResult[1]['list_address_lat'],
					$orderResult[1]['list_address_lon']
				);
				$distance = round($infoRoute['routes'][0]['distance']/1000, 3);
				$minutes = round($infoRoute['routes'][0]['duration']/60, 1);
				// $priceKM = 3.8;
				$priceKM = 8;
				$price = ceil(($distance*$priceKM)*(1.08/($distance/$minutes))+50);
				if ($taxi = $this->getTaxiById($this->id_base)) {
					$infoRoute = $this->getInfoRoute(
						$taxi['lat'],
						$taxi['lon'],
						$orderResult[0]['list_address_lat'],
						$orderResult[0]['list_address_lon']
					);
					$distanceToClient = round($infoRoute['routes'][0]['distance']/1000, 3);
				}
				$info =
					'Заявка №*'.$order.PHP_EOL.'*'.
					'Имя клиента: *'.$orderResult[0]['user_name'].'*'.PHP_EOL.
					'Телефон клиента: *'.$orderResult[0]['user_phone'].'*'.PHP_EOL.
					'Расстояние до клиента: *'.$distanceToClient.' км'.'*'.PHP_EOL.PHP_EOL.
					'Начальный адрес:*'.PHP_EOL.$orderResult[0]['list_address_name'].'*'.PHP_EOL.PHP_EOL.
					'Конечный адрес:*'.PHP_EOL.$orderResult[1]['list_address_name'].'*'.PHP_EOL.PHP_EOL.
	
					'Расстояние: *'.$distance.' км*'.PHP_EOL.
					'Время в пути: *'.$minutes.' минут*'.PHP_EOL.
					'Стоимость поездки: *'.$price.' руб.*'.PHP_EOL.
					'Способ оплаты: *наличные*'.PHP_EOL.PHP_EOL.

					// 'Местоположение клиента в следующем сообщении'
					'Местоположение клиента:'
				;
				return array(
					'msg' => $info,
					'location' => array(
						'lat' => $orderResult[0]['list_address_lat'],
						'lon' => $orderResult[0]['list_address_lon']
					)
				);
			} else {
				return false;
			}
		}

		/**
		 * Отправлен ли кому-нибудь другому вопрос о взятии заявки за последние 15 секунд
		 * @order - id заявки
		 * @me - мне ли
		 * @id_base - id таксиста
		 */
		function isSendOrderToTaxi($order, $me, $id_base) {
			$query = "
				SELECT
					*
				FROM
					`sendOrdersToTaxi`
				WHERE
					id_order = '{$order}'
			";
			if ($me == false) {
				$query .= "
					AND send_time > '".(time() - 15)."'
				";
				$query .= "
						AND send_response != 1
				";
				$query .= "
						AND id_taxi != {$id_base}
				";
			} else {
				$query .= "
						AND id_taxi = {$id_base}
				";
			}

			$res = $this->db->select($query);
			if (count($res) > 0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Отмечаем в базе, что отправили таксисту заказ
		 * @order - id заказа
		 * @id_base - id таксиста в базе
		 */
		function sendOrderToTaxi($order, $id_base) {
			




			// if ($this->id_base == $id_base) {
				if (!($this->isSendOrderToTaxi($order, false, $id_base))) {
					// if ($this->updateUserStatus($this->id_base, 32)) {
					if ($this->updateUserStatus($id_base, 32)) {
						if (!($this->isSendOrderToTaxi($order, true, $id_base))) {
							$query = "
								INSERT INTO
									`sendOrdersToTaxi`
								(
									`send_id`,
									`send_time`,
									`id_taxi`,
									`id_order`,
									`send_response`
								)
								VALUES
								(
									NULL,
									'".time()."',
									'{$id_base}',
									'{$order}',
									'0'
								)
							";
						} else {
							$query = "
								UPDATE
									`sendOrdersToTaxi`
								SET
									`send_response` = '0',
									`send_time` = '".time()."'
								WHERE
									(id_taxi = {$id_base})
									AND (id_order = {$order})
								ORDER BY send_id DESC
								LIMIT 1
							";
						}

						// $this->logPr(
						// 	array(
						// 		'UPDATEQ' => $query
						// 	)
						// );



						// $method = 'sendMessage';
						// $send_data = [
						// 	'chat_id' => '1573720701',
						// 	'text' => 'log:'.PHP_EOL.
						// 		'$query: '.$query
						// ];
						// $this->sendTelegram($method, $send_data, true);











						$result = $this->db->insert($query);
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
			// } else {
			// 	return true;
			// }
		}

		/**
		 * Я нажал взять в работу
		 */
		function inWork() {
			// Найти id заказа, который я (таксист) взял
			if ($order = $this->getLastSendOrderToTaxist()) {
				// Проверить, существует ли он еще в базе и активен ли он
				if ($this->issetActiveOrder($order)) {
					// Сначала проверить, не взял ли кто-нибудь другой мой заказ
					if (!($this->orderIsAccepted($order))) {
						// Все нормально, берем заказ
						if ($this->updateOrderStatus($order, 2)) {
							// Получаем id заказчика
							if ($client_id = $this->getClientIdFromOrder($order)) {
								// Обновляем клиенту статус
								if ($this->updateUserStatus($client_id, 11)) {
									// И отсылаем сообщение клиенту о машине
									if ($this->sendTaxiToClient($order)) {
										// Составляем полное сообщение о заявке для таксиста
										if ($msg = $this->getInfoOrderToTaxist($order)) {
											// return array(
											// 	'message' => $msg['msg'],
											// 	'btn' => $this->getCommands(33)['btn']
											// );

											$method = 'sendMessage';
											$send_data = [
												'chat_id' => $this->id_tg,
												'text' => $msg['msg'],
												'parse_mode' => 'Markdown'
												// 'text' => 'ЫЫЫ'
											];
											$this->sendTelegram($method, $send_data, true);

											$method = 'sendLocation';
											$send_data = [
												'chat_id' => $this->id_tg,
												'latitude' => $msg['location']['lat'],
												'longitude' => $msg['location']['lon'],
												'reply_markup' => $this->generateButtons(
													$this->getCommands(33)['btn']
												)
												
												
												//json_encode($inlineKeyboardMarkup)
												// 'chat_id' => $this->id_tg,
												// 'text' => $msg['msg']//.json_encode($_SESSION)
											];
											$this->sendTelegram($method, $send_data, true);


											// $method = 'sendMessage';
											// $send_data = [
											// 	'chat_id' => $this->id_base,
											// 	'text' => $msg['msg']
											// ];
											// if ($this->sendTelegram($method, $send_data, true)) {
											// 	// return true;
											// 	return array(
											// 		'message' => 'Вроде бы получилось',
											// 		'btn' => $this->getCommands(33)['btn']
											// 	);
											// } else {
											// 	return array(
											// 		'message' => 'Че то не получилось',
											// 		'btn' => $this->getCommands(33)['btn']
											// 	);
											// }

										} else {
											return array(
												'message' => '7ы',
												'btn' => $this->getCommands(31)['btn']
											);
										}
									} else {
										return array(
											'message' => '6ы',
											'btn' => $this->getCommands(31)['btn']
										);
									}
								} else {
									return array(
										'message' => '5ы',
										'btn' => $this->getCommands(31)['btn']
									);
								}
							} else {
								return array(
									'message' => '4ы',
									'btn' => $this->getCommands(33)['btn']
								);
							}
						} else {
							return array(
								'message' => '3ы',
								'btn' => $this->getCommands(33)['btn']
							);
						}
					}
				} else {
					return array(
						'message' => '2ы',
						'btn' => $this->getCommands(33)['btn']
					);
				}
			} else {
				return array(
					'message' => '1ы',
					'btn' => $this->getCommands(31)['btn']
				);
			}
		}

		/**
		 * Найти id заказа, на который я (таксист) еще не ответил
		 */
		function getLastSendOrderToTaxist() {
			$query = "
				SELECT
					*
				FROM
					`sendOrdersToTaxi`
				WHERE
					`id_taxi` = {$this->id_base} AND
					`send_response` = '0'
				ORDER BY send_id DESC
				LIMIT 1
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return $res[0]['id_order'];
			} else {
				return false;
			}
		}

		/**
		 * Найти id заказа, на который я (таксист) отвечал, что взял
		 */
		function getLastSendOrderToTaxistYes() {
			$query = "
				SELECT
					*
				FROM
					`sendOrdersToTaxi`
				WHERE
					`id_taxi` = {$this->id_base} AND
					`send_response` = '2'
				ORDER BY send_id DESC
				LIMIT 1
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return $res[0]['id_order'];
			} else {
				return false;
			}
		}

		/**
		 * Найти id заказа, на который я (таксист) отвечал, что взял и отметить, что send_response - 3 (заказ выполнен)
		 */
		function successLastSendOrderToTaxistYes() {
			if ($order = $this->getLastSendOrderToTaxistYes()) {
				$query = "
					UPDATE
						`sendOrdersToTaxi`
					SET
						`send_response` = '3'
					WHERE
						`id_order` = {$order}
				";
				$this->db->select($query);
				return true;
			} else {
				return true;
			}
		}

		/**
		 * Проверка существования заказа в базе
		 * @order - id заявки
		 */
		function issetOrder($order) {
			$query = "
				SELECT
					*
				FROM
					`orders`
				WHERE
					`order_id` = {$order}
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Проверка существования активного заказа в базе
		 * @order - id заявки
		 */
		function issetActiveOrder($order) {
			$query = "
				SELECT
					*
				FROM
					`orders`
				WHERE
					`order_id` = {$order} AND
					`order_active` = 1 AND
					`order_success` = 0
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Взял ли кто-нибудь заказ
		 */
		function orderIsAccepted($order) {
			$query = "
				SELECT
					*
				FROM
					`sendOrdersToTaxi`
				WHERE
					`id_order` = {$order} AND
					`send_response` = 2
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return true;
			} else {
				return false;
			}
		}

		/**
		 * Обновляем статус заказа
		 */
		function updateOrderStatus($order, $status) {
			$query = "
				UPDATE
					`sendOrdersToTaxi`
				SET
					`send_response` = '{$status}'
				WHERE
					`id_order` = {$order}
			";
			$res = $this->db->select($query);
			return true;
		}

		/**
		 * Отсылаем сообщение клиенту о машине
		 */
		function sendTaxiToClient($order) {
			// Получаем полную информацию о заказе и водителе
			$query = "
				SELECT
					*,
					orders.user_id AS 'order_user'
				FROM
					sendOrdersToTaxi
					LEFT JOIN orders ON orders.order_id = sendOrdersToTaxi.id_order
					LEFT JOIN order_list ON orders.order_id=order_list.order_id
					LEFT JOIN users ON users.user_id = sendOrdersToTaxi.id_taxi
					LEFT JOIN taxi ON taxi.user_id = users.user_id
					
				WHERE
					`id_order` = {$order}
			";
			$res = $this->db->select($query);
			if (count($res) == 2) {

				$msg =
					"Автомобиль назначен.".PHP_EOL.
					"Через 5 минут за тобой приедет:".PHP_EOL.
					"{$res[0]['auto_color']} {$res[0]['auto_mark']} {$res[0]['auto_model']}, {$res[0]['auto_numbers']}".PHP_EOL.
					"Водитель: {$res[0]['user_name']}".PHP_EOL.
					"Номер телефона: {$res[0]['user_phone']}"
				;
				$method = 'sendMessage';
				$send_data = [
					// 'chat_id' => $res[0]['order_user'],
					'chat_id' => $this->getUser($res[0]['order_user'])['user_tg_id'],
					// 'chat_id' => $this->id_tg,
					'text' => $msg,
					'parse_mode' => 'Markdown',
					'reply_markup' => $this->generateButtons(
						$this->getCommands(11)['btn']
					)
				];

				// return $this->sendTelegram($method, $send_data, false);
				return $this->sendTelegram($method, $send_data, true);

			} else {
				return false;
			}
		}

		/**
		 * Отмена заказа клиентом
		 */
		function canselOrder() {
			return true;
		}

		/**
		 * Отмена заказа таксистом
		 */
		function canselOrderTaxist() {
			// if ($this->rejectAllOrders()) {
			// 	return true;
			// }
			// Найти id заказа, который я хочу отменить
			// Найти id заказа, который я (таксист) взял
			if ($order = $this->getLastSendOrderToTaxistYes()) {
				// Получаем id заказчика
				if ($client_id = $this->getClientIdFromOrder($order)) {
					// Обновляем клиенту статус
					if ($this->updateUserStatus($client_id, 7)) {

						$method = 'sendMessage';
						$send_data = [
							'chat_id' => $this->getUser($client_id)['user_tg_id'],
							// 'chat_id' => $this->id_tg,
							'text' => 'Таксист отказался от твоего заказа, ищу другую машину',
							'reply_markup' => $this->generateButtons(
								$this->getCommands(7)['btn']
							)
						];
						// if ($this->sendTelegram($method, $send_data, false)) {
						if ($this->sendTelegram($method, $send_data, true)) {
							$query = "
								UPDATE
									`sendOrdersToTaxi`
								SET
									`send_response` = '1'
								WHERE
									(id_taxi = {$this->id_base}) AND
									(`send_response` = '2')
							";
							$result = $this->db->select($query);
							return true;
						}
					}
				}
			} else {
				return true;
			}
		}

		/**
		 * Получаем id заказчика
		 */
		function getClientIdFromOrder($order) {
			$query = "
				SELECT
					user_id
				FROM
					orders
				WHERE
					order_id = {$order}
			";
			$res = $this->db->select($query);
			if (count($res) > 0) {
				return $res[0]['user_id'];
			} else {
				return false;
			}
		}


		/**
		 * Получаем финишную точку
		 */
		function getFinishPoint() {
			if ($order = $this->getLastSendOrderToTaxistYes()) {
				// Получаем полную информацию о заказе и водителе
				$query = "
					SELECT
						*,
						orders.user_id AS 'order_user'
					FROM
						sendOrdersToTaxi
						LEFT JOIN orders ON orders.order_id = sendOrdersToTaxi.id_order
						LEFT JOIN order_list ON orders.order_id=order_list.order_id
						LEFT JOIN users ON users.user_id = sendOrdersToTaxi.id_taxi
						-- LEFT JOIN taxi ON taxi.user_id = users.user_id
						
					WHERE
						`id_order` = {$order}
				";
				if ($orderResult = $this->db->select($query)) {


					
					$method = 'sendMessage';
					$send_data = [
						'chat_id' => $this->id_tg,
						'text' => 'Конечный пункт:'.PHP_EOL.$orderResult[1]['list_address_name'],
						'parse_mode' => 'Markdown',
					];
					$this->sendTelegram($method, $send_data, true);




					$method = 'sendLocation';
					$send_data = [
						'chat_id' => $this->id_tg,
						'latitude' => $orderResult[1]['list_address_lat'],
						'longitude' => $orderResult[1]['list_address_lon'],
						'reply_markup' => $this->generateButtons(
							$this->getCommands(34)['btn']
						)
					];
					$this->sendTelegram($method, $send_data, true);
				}
			}

		}

		/**
		 * Для водителя сообщение "оцени пассажира"
		 */
		function rateThePassenger() {
			if ($order = $this->getLastSendOrderToTaxistYes()) {
				// Получаем информацию о заказе и пассажире
				$query = "
					SELECT
						*
					FROM
						users
						LEFT JOIN orders ON orders.user_id = users.user_id
					WHERE
						orders.order_id = {$order}
				";
				if ($user = $this->db->select($query)) {
					if ($this->updateUserStatus($user[0]['user_id'], 12)) {
						$method = 'sendMessage';
						$send_data = [
							'chat_id' => $user[0]['user_tg_id'],
							'text' => 'Оцени водителя',
							'reply_markup' => $this->generateButtons(
								$this->getCommands(12)['btn']
							)
						];
						if ($this->sendTelegram($method, $send_data, true)) {
							return array(
								'message' => 'Оцени пассажира',
								'btn' => $this->getCommands(35)['btn']
							);
						}
					}
				}
			}
		}


		/**
		 * Отсылаем сообщение
		 */
		function sendTelegram($method, $data, $yes = false) {
			if ($yes == true) {
				$ch = curl_init('https://api.telegram.org/bot' . '1449831442:AAEXW4ET1cqLegF_cZccKLd-F-5Jup0ve4g' . '/' . $method);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_exec($ch);
				curl_close($ch);
			}
			return true;
		}

	}