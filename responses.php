<?

	/*// Старый массив с вопросами
	public $responses = array(
		1 => array(
			'message' => 'Добро пожаловать в такси-бот! Как мне тебя называть?',
			'function' => 'updateName',
			'nextCommand' => '2',
		),
		2 => array(
			'message' => 'Ок, понял. Напиши свой номер телефона - это нужно для связи водителя с тобой',
			'function' => 'updatePhone',
			'nextCommand' => '3',
		),
		// 3 => array(
		// 	'message' => 'Выбери, какой услугой ты хочешь пользоваться',
		// 	'function' => '',
		// 	'nextCommand' => '4',
		// ),
		4 => array(
			'message' => 'Закажем такси? Отправь мне адрес, где ты находишься',
			'function' => 'setGeoA',
			'nextCommand' => '5',
		),
		5 => array(
			'message' => 'Отлично, отправь мне адрес или местоположение, куда тебе нужно',
			'function' => 'setGeoB',
			'nextCommand' => '6',
		),
		6 => array(
			'message' => 'Отлично, ожидай машину',
			// в функции сделать возврат рассчета стоимости по таксометру
			'function' => 'setGeoB',
			'nextCommand' => '6',
		),
	);*/

	// Новый массив с вопросами
	return array(
		/**
		 * Приветствие и запись имени
		 */
		1 => array( // номер массива - статус человека
			// стартовое сообщение (в случае несовпадения статуса человека с номером массива)
			'msg' => array(
				// это может быть функция
				'text' => 'Привет! Напиши свое имя',
			),
			// сообщение об ошибке
			'error' => array(
				'text' => 'Такое имя не подойдет'
			),
			// массив с кнопками
			'responses' => array(),
			// функция этого элемента может и не быть
			'func' => array(
				// наименование функции, к которой обращаемся (функция не может изменить статус человека).
				'name' => 'updateName',
				// переход к следующему шагу (этого элемента может и не быть)
				'next' => 2
			),
		),

		/**
		 * Запись телефона
		 */
		2 => array(
			'msg' => array(
				'text' => 'Напиши свой номер телефона (пример: 88008008080) или нажми "Отправить свой номер"'
			),
			'error' => array(
				'text' => 'Это не похоже на номер'
			),
			'responses' => array(
				'con' => array(
					'name' => 'con',
					'next' => 3
				),
			),
			'func' => array(
				'name' => 'updatePhone',
				'next' => 3
			),
		),

		/**
		 * Выбери, как ты хочешь воспользоваться ботом
		 */
		3 => array(
			'msg' => array(
				'text' => 'Выбери, как ты хочешь воспользоваться ботом',
			),
			'error' => array(
				'text' => 'Такого варианта ответа нет'
			),
			// ответы от пользователя (этого элемента может и не быть)
			'responses' => array(
				'Вызвать такси' => array(
					'name' => 'Вызвать такси',
					'next' => 4
				),
				'Стать водителем' => array(
					'name' => 'Стать водителем',
					'next' => 24
				),
				'Мой профиль' => array(
					'name' => 'Мой профиль',
					'next' => 8
				),
			),
		),

		/**
		 * Часть для пассажиров
		 */
		




		/**
		 * Где ты находишься
		 */
		4 => array(
			'msg' => array(
				'text' => 'Где ты находишься?',
			),
			'error' => array(
				'text' => 'Не могу найти такой адрес. Напиши город, улицу и дом (например: Уфа королева 4)'
			),
			'func' => array(
				'name' => 'setPointA',
				'next' => 5
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'func' => array(
						'name' => 'deActivateOrder',
						'next' => 3
					),
				),
			),
		),

		/**
		 * Куда тебе нужно
		 */
		5 => array(
			'msg' => array(
				'text' => 'Куда тебе нужно?',
			),
			'error' => array(
				'text' => 'Не могу найти такой адрес. Напиши город, улицу и дом (например: Уфа королева 4)'
			),
			'func' => array(
				'name' => 'setPointB',
				'next' => 6
			),
			'responses' => array(
				'Назад' => array(
					'name' => 'Назад',
					'next' => 4
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'func' => array(
						'name' => 'deActivateOrder',
						'next' => 3
					),
				),
			),
		),

		/**
		 * Создать ли заявку
		 */
		6 => array(
			'msg' => array(
				'func' => 'getInfoLastOrder'
			),
			'error' => array(
				'text' => 'Такого варианта ответа нет'
			),
			'responses' => array(
				'Да' => array(
					'name' => 'Да',
					'func' => array(
						'name' => 'activateOrder',
						'next' => 7
					),
				),
				'Назад' => array(
					'name' => 'Назад',
					'next' => 5
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'func' => array(
						'name' => 'deActivateOrder',
						'next' => 3
					),
				),
			),
		),

		/**
		 * Заявка создана
		 */
		7 => array(
			'msg' => array(
				'text' => 'Заявка создана, ищу машину'
			),
			'error' => array(
				'text' => 'Ищу машину err'
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'func' => array(
						'name' => 'deActivateOrder',
						'next' => 3
					),
				),
			),
		),

		/**
		 * Мой профиль
		 */
		8 => array(
			'msg' => array(
				'func' => 'getProfileInfo'
			),
			'error' => array(
				'text' => 'Такого варианта ответа нет'
			),
			'responses' => array(
				'Изменить имя' => array(
					'name' => 'Изменить имя',
					'next' => 9
				),
				'Изменить номер' => array(
					'name' => 'Изменить номер',
					'next' => 10
				),
				'Назад' => array(
					'name' => 'Назад',
					'next' => 3
				),
			),
		),

		/**
		 * Изменить имя
		 */
		9 => array(
			'msg' => array(
				'text' => 'Напиши свое имя'
			),
			'error' => array(
				'text' => 'Напиши имя правильно'
			),
			'func' => array(
				'name' => 'updateName',
				'next' => 8
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 8
				),
			),
		),

		/**
		 * Изменить номер
		 */
		10 => array(
			'msg' => array(
				'text' => 'Напиши свой номер телефона (пример: 88008008080) или нажми "Отправить свой номер"'
			),
			'error' => array(
				'text' => 'Напиши номер правильно'
			),
			'func' => array(
				'name' => 'updatePhone',
				'next' => 8
			),
			'responses' => array(
				'con' => array(
					'name' => 'con',
					'next' => 8
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 8
				),
			),
		),

		/**
		 * Скоро приедет машина
		 */
		11 => array(
			'msg' => array(
				'text' => 'Скоро приедет машина'
			),
			'error' => array(
				'text' => 'Скоро приедет машина'
			),
			'responses' => array(
				'Отменить заказ' => array(
					'name' => 'Отменить заказ',
					'func' => array(
						'name' => 'canselOrder',
						'next' => 3
					),
				),
			),
		),

		/**
		 * Поездка завершена. Оценить водителя
		 */
		12 => array(
			'msg' => array(
				// 'func' => 'getFinishPoint',
				'text' => 'Оцени водителя'
			),
			'error' => array(
				'text' => 'Надо поставить оценку водителя'
			),
			'responses' => array(
				'👍' => array(
					'name' => '👍',
					'next' => '3',
				),
				'👎' => array(
					'name' => '👎',
					'next' => '13',
				),
			)
		),

		/**
		 * Плохо оценил
		 */
		13 => array(
			'msg' => array(
				'text' => 'Я с ним разберусь..'
			),
			'error' => array(
				'text' => 'Я с ним разберусь..'
			),
			'responses' => array(
				'Ок' => array(
					'name' => 'Ок',
					'next' => '3',
				),
			)
		),


















		/**
		 * Часть для таксистов
		 */




		/**
		 * Запись марки
		 */
		20 => array(
			'msg' => array(
				'text' => 'Напиши марку авто (например: lada, bmw, skoda)',
			),
			'error' => array(
				'text' => 'Напиши только марку авто'
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 3
				),
			),
			'func' => array(
				'name' => 'updateMark',
				'next' => 21
			),
		),

		/**
		 * Запись модели
		 */
		21 => array(
			'msg' => array(
				'text' => 'Напиши модель авто (например: 2109, m5, oktavia)',
			),
			'error' => array(
				'text' => 'Напиши только модель авто'
			),
			'responses' => array(
				'Назад' => array(
					'name' => 'Назад',
					'next' => 20
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 3
				),
			),
			'func' => array(
				'name' => 'updateModel',
				'next' => 22
			),
		),

		/**
		 * Запись цвета
		 */
		22 => array(
			'msg' => array(
				'text' => 'Напиши цвет авто (например: белый, черный, серебристый)',
			),
			'error' => array(
				'text' => 'Напиши только цвет авто'
			),
			'responses' => array(
				'Назад' => array(
					'name' => 'Назад',
					'next' => 21
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 3
				),
			),
			'func' => array(
				'name' => 'updateColor',
				'next' => 23
			),
		),

		/**
		 * Запись номеров
		 */
		23 => array(
			'msg' => array(
				'text' => 'Напиши номера авто (например: А111АА999)',
			),
			'error' => array(
				'text' => 'Напиши номера авто'
			),
			'responses' => array(
				'Назад' => array(
					'name' => 'Назад',
					'next' => 22
				),
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 3
				),
			),
			'func' => array(
				'name' => 'updateNumbers',
				'next' => 24
			),
		),

		/**
		 * Готов ли работать
		 */
		24 => array(
			'msg' => array(
				'text' => 'Готов ли работать?',
			),
			'error' => array(
				'text' => 'Готов ли работать?'
			),
			'responses' => array(
				'Ожидать заявки' => array(
					'name' => 'Ожидать заявки',
					'next' => 25
				),
				// '👍' => array(
				// 	'name' => '👍',
				// 	'next' => 25
				// ),
				'Мой авто' => array(
					'name' => 'Мой авто',
					'next' => 26
				),
				'Вернуться в меню' => array(
					'name' => 'Вернуться в меню',
					'next' => 3
				),
			),
		),

		/**
		 * Отправь свою геопозицию
		 */
		25 => array(
			'msg' => array(
				'text' => 'Отправь свою геопозицию',
			),
			'error' => array(
				'text' => 'Нажми "Отправить свое местоположение"'
			),
			'func' => array(
				'name' => 'updateMyGeo',
				'next' => 31
			),
			'responses' => array(
				'loc' => array(
					'name' => 'loc',
					// 'next' => 25
				),
				'Назад' => array(
					'name' => 'Назад',
					'next' => 24
				),
			),
		),

		/**
		 * Мой авто
		 */
		26 => array(
			'msg' => array(
				// 'text' => 'Мой авто',
				'func' => 'getAutoInfo',
			),
			'error' => array(
				'text' => 'Такой команды нет'
			),
			'responses' => array(
				'Изменить марку' => array(
					'name' => 'Изменить марку',
					'next' => 27
				),
				'Изменить модель' => array(
					'name' => 'Изменить модель',
					'next' => 28
				),
				'Изменить цвет' => array(
					'name' => 'Изменить цвет',
					'next' => 29
				),
				'Изменить номера' => array(
					'name' => 'Изменить номера',
					'next' => 30
				),
				'Назад' => array(
					'name' => 'Назад',
					'next' => 24
				),
			),
		),
		
		/**
		 * Изменить марку
		 */
		27 => array(
			'msg' => array(
				'text' => 'Напиши марку авто (например: lada, bmw, skoda)'
			),
			'error' => array(
				'text' => 'Напиши марку авто правильно'
			),
			'func' => array(
				'name' => 'updateMark',
				'next' => 26
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 26
				),
			),
		),

		/**
		 * Изменить модель
		 */
		28 => array(
			'msg' => array(
				'text' => 'Напиши модель авто (например: 2109, m5, oktavia)'
			),
			'error' => array(
				'text' => 'Напиши модель авто правильно'
			),
			'func' => array(
				'name' => 'updateModel',
				'next' => 26
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 26
				),
			),
		),

		/**
		 * Изменить цвет
		 */
		29 => array(
			'msg' => array(
				'text' => 'Напиши цвет авто (например: белый, черный, серебристый)'
			),
			'error' => array(
				'text' => 'Напиши цвет авто правильно'
			),
			'func' => array(
				'name' => 'updateColor',
				'next' => 26
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 26
				),
			),
		),

		/**
		 * Изменить номера
		 */
		30 => array(
			'msg' => array(
				'text' => 'Напиши номера авто (например: А111АА999)'
			),
			'error' => array(
				'text' => 'Напиши номера авто правильно'
			),
			'func' => array(
				'name' => 'updateNumbers',
				'next' => 26
			),
			'responses' => array(
				'Отмена' => array(
					'name' => 'Отмена',
					'next' => 26
				),
			),
		),

		/**
		 * Ищем заявку
		 */
		31 => array(
			'msg' => array(
				'func' => 'getOrder',
				// 'text' => 'Ищем для тебя заявку',
			),
			'error' => array(
				'text' => 'Ищем для тебя заявку err'
			),
			'responses' => array(
				'Завершить поиск' => array(
					'name' => 'Завершить поиск',
					'func' => array(
						'name' => 'deactiveTaxist',
						'next' => 25
					),
				),
			),
		),

		/**
		 * Заявка
		 */
		32 => array(
			'error' => array(
				'text' => 'Какая-то ошибка'
			),
			'responses' => array(
				'Взять в работу' => array(
					'name' => 'Взять в работу',
					'next' => '33',
				),
				'Переназначить' => array(
					'name' => 'Переназначить',
					'next' => '31',
					// 'func' => array(
					// 	'name' => 'getOrder',
					// 	'next' => 
					// )
				),
				'Завершить поиск' => array(
					'name' => 'Завершить поиск',
					'func' => array(
						'name' => 'deactiveTaxist',
						'next' => 24
					),
				),
				// 'Завершить поиск' => array(
				// 	'name' => 'Завершить поиск',
				// 	'next' => '24',
				// )
			)
		),

		/**
		 * Полная информация о заявке с первой картой
		 */
		33 => array(
			'msg' => array(
				'func' => 'inWork',
			),
			'error' => array(
				'text' => 'Какая-то ошибка'
			),
			'responses' => array(
				'Показать конечный пункт' => array(
					'name' => 'Показать конечный пункт',
					'next' => '34',
				),
				'Отменить заказ' => array(
					'name' => 'Отменить заказ',
					'next' => '31',
				),
			)
		),

		/**
		 * Показать последний пункт
		 */
		34 => array(
			'msg' => array(
				'func' => 'getFinishPoint',
			),
			'error' => array(
				'text' => 'Какая-то ошибка'
			),
			'responses' => array(
				'Поездка завершена' => array(
					'name' => 'Поездка завершена',
					'next' => '35',
				),
				// 'Завершить работу' => array(
				// 	'name' => 'Завершить работу',
				// 	'func' => array(
				// 		'name' => 'deactiveTaxist',
				// 		'next' => 3
				// 	),
				// ),
			)
		),

		/**
		 * Поездка завершена. Оценить пассажира
		 */
		35 => array(
			'msg' => array(
				'func' => 'rateThePassenger',
				// 'text' => 'Оцени пассажира'
			),
			'error' => array(
				'text' => 'Надо поставить оценку пассажиру'
			),
			'responses' => array(
				'👍' => array(
					'name' => '👍',
					'next' => '37',
				),
				'👎' => array(
					'name' => '👎',
					'next' => '36',
				),
				// 'Пассажир не заплатил' => array(
				// 	'name' => 'Пассажир не заплатил',
				// 	'next' => '35',
				// ),
			)
		),

		/**
		 * Плохо оценил
		 */
		36 => array(
			'msg' => array(
				'text' => 'Я с ним разберусь..'
			),
			'error' => array(
				'text' => 'Я с ним разберусь..'
			),
			'responses' => array(
				'Ок' => array(
					'name' => 'Ок',
					'next' => '37',
				),
				'Завершить работу' => array(
					'name' => 'Завершить работу',
					'func' => array(
						'name' => 'deactiveTaxist',
						'next' => 3
					),
				),
			)
		),

		

		/**
		 * Правильно завершаем и ищем заявку
		 */
		37 => array(
			'msg' => array(
				'func' => 'getNewOrder',
				// 'text' => 'Ищем для тебя заявку',
			),
			'error' => array(
				'text' => 'Ищем для тебя заявку err'
			),
			'responses' => array(
				'Завершить поиск' => array(
					'name' => 'Завершить поиск',
					'func' => array(
						'name' => 'deactiveTaxist',
						'next' => 25
					),
				),
			),
		),

	);